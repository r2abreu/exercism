# frozen_string_literal: true

class Tournament
  # To doc
  class Team
    attr_reader :name, :stats

    def initialize(name)
      @name = name
      @stats = {
        played: 0,
        won: 0,
        draw: 0,
        lost: 0,
        points: 0
      }
    end

    def update(game)
      @stats[:played] += 1
      return draw if game.result == 'draw'

      @name == game.winner ? win : loss
    end

    def win
      @stats[:won] += 1
      @stats[:points] += 3
    end

    def draw
      @stats[:draw] += 1
      @stats[:points] += 1
    end

    def loss
      @stats[:lost] += 1
    end

    def columns
      [@name, *@stats.values]
    end
  end
end
