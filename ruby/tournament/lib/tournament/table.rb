# frozen_string_literal: true

class Tournament
  # Formats and prints a table of teams and their stats.
  class Table
    def initialize(input)
      @header_row = %w[Team MP W D L P]
      @rows = input
    end

    def print
      sort_by_points_and_name
        .map(&:columns)
        .unshift(@header_row)
        .map { |row| format_row(row) }
        .join
    end

    def format_row(columns)
      formatted = columns.map.with_index do |column, index|
        if index.zero?
          format('%-31s', column)
        elsif columns.length == index + 1
          format('% 3s', column)
        else
          format('% 3s ', column)
        end
      end

      formatted.join('|').concat("\n")
    end

    def sort_by_points_and_name
      @rows.sort_by { |row| [-row.stats[:points], row.name] }
    end
  end
end
