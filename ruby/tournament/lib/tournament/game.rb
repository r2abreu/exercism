# frozen_string_literal: true

class Tournament
  # Sports game with home and away teams and a result,
  # allowing access to teams, winner, and result.
  class Game
    attr_reader :home, :away, :result

    def initialize(game)
      @home = home!(game)
      @away = away!(game)
      @result = result!(game)
    end

    def winner
      @result == 'win' ? @home : @away
    end

    def teams
      [@home, @away]
    end

    def home!(game)
      game.split(';')[0]
    end

    def away!(game)
      game.split(';')[1]
    end

    def result!(game)
      game.split(';')[2]
    end
  end
end
