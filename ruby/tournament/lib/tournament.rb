# frozen_string_literal: true

require_relative 'tournament/table'
require_relative 'tournament/team'
require_relative 'tournament/game'
require 'byebug'

# Used by task runner
class Tournament
  attr_reader :teams

  def self.tally(games)
    new(games).print_table
  end

  def new(games)
    obj.initialize(games)
    obj
  end

  def initialize(games)
    @teams = []
    games.strip.each_line(chomp: true) { |game| register(game) }
  end

  def register(game)
    game_instance = Game.new game

    game_instance.teams.each do |team|
      stored_team = find_team_by_name(team)
      if stored_team.nil?
        stored_team = Team.new team
        @teams << stored_team
      end
      stored_team.update(game_instance)
    end
  end

  def find_team_by_name(name)
    @teams.find { |team| team.name == name }
  end

  def print_table
    Table.new(@teams).print
  end
end
